package ch.hevs.isi.utils;

import ch.hevs.isi.core.BoolDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.database.DataBaseConnector;
import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FloatRegister;
import ch.hevs.isi.field.ModbusAccessor;

import org.json.JSONArray;
import org.json.JSONObject;



import java.io.*;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This class contains some useful Java methods to manipulate Modbus data.
 *
 * @author Michael Clausen
 * @author Patrice Rudaz
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class Utility {

    /**
     * Calculates and returns the CRC using the data passed in parameters.
     *
     * @param data a byte array containing the data to send
     * @param offset the offset
     * @param len the data length
     * @return byte[] the CRC
     */
    public static byte[] calculateCRC(byte[] data , int offset , int len)
    {
        int crc = 0x0000FFFF;
        for (int i = 0 ; i < len ; i++)
        {
            crc = crc ^ Utility.unsignedByteToSignedInt(data[i + offset]);
            for (int j = 0 ; j < 8 ; j++)
            {
                int tmp = crc;
                int carryFlag = tmp & 0x0001;
                crc = crc >> 1;
                if (carryFlag == 1)
                {
                    crc = crc ^ 0xA001;
                }
            }
        }

        byte[] result = new byte[2];
        result[0] = (byte)(crc & 0xFF);
        result[1] = (byte)((crc & 0xFF00) >> 8);

        return result;
    }

    /**
     * Checks the CRC and returns true if it is correct, otherwise false.
     *
     * @param data a byte array containing the data to send
     * @param offset the offset
     * @param len the data length
     * @param crc a byte array containing the CRC to check
     * @return boolean true if the CRC is correct, otherwise false
     */
    public static boolean checkCRC(byte[] data , int offset , int len , byte[] crc)
    {
        byte[] calcCrc = Utility.calculateCRC(data , offset , len);
        if (calcCrc[0] == crc[0] && calcCrc[1] == crc[1])
            return true;
        else
            return false;
    }


    /**
     * Converts an unsigned byte to a signed integer.
     *
     * @param from an unsigned byte to convert to a signed integer
     * @return int a signed integer
     */
    public static int unsignedByteToSignedInt(byte from)
    {
        return 0x000000FF & (int)from;
    }

    /**
     * Utility method to convert a byte array in a string made up of hex (0,.. 9, a,..f)
     * @param b byte array
     * @throws Exception exception
     * @return result
     */
    public static String getHexString(byte[] b) throws Exception {
        return getHexString(b, 0, b.length);
    }

    public static String getHexString(byte[] b, int offset, int length) {
        String result = "";
        for (int i = offset ; i < offset+length ; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1) ;
        }
        return result;
    }


    /**
     * To wait some times ...
     * @param ms time in milliseconds
     */
    public static void waitSomeTime(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            DEBUG("Utility", "waitSomeTime()", "Exception : " + e.getMessage());
        }
    }

    /**
     * Returns the md5 of any input...
     * @param msg The input string to process
     * @return  The md5 of the input string.
     */
    public static String md5sum(String msg)
    {
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            return String.format("%032x", new BigInteger(1, md.digest(msg.getBytes("UTF-8"))));
        } catch (UnsupportedEncodingException e) {
            DEBUG("Utility", "md5sum()", "UnsupportedEncodingException");
        } catch (NoSuchAlgorithmException e) {
            DEBUG("Utility", "md5sum()", "NoSuchAlgorithmException");
        }
        return null;
    }

    /**
     * Returns a <code>float</code> value from array of bytes. This byte's array can only be 2 or 4 bytes long.
     * @param bytes     The array of bytes to convert.
     * @param offset    The position where the method has to start to get the bytes from.
     * @param size      The amount of bytes to convert.
     * @return A <code>Float</code> value or <code>null</code> if the process failed.
     */
    public static Float bytesToFloat(byte[] bytes, int offset, int size) {

        if (size == 2 || size == 4) {
            byte[] tmp = new byte[4];
            System.arraycopy(bytes, offset, tmp, 0, size);
            try {
                return ByteBuffer.allocate(4).wrap(tmp).order(ByteOrder.BIG_ENDIAN).getFloat();
            } catch (Exception e) {
                DEBUG("Utility", "bytesToFloat()", "ByteBufferException: " + e.getLocalizedMessage());
            }
        } else {
            DEBUG("Utility", "bytesToFloat()", "ERROR: size MUST be 2 or 4 !!!");
        }
        return null;
    }

    // DEBUG System.out
    public static void DEBUG(String className, String method, String msg) {
        int millis = Calendar.getInstance().get(Calendar.MILLISECOND);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        System.out.println(sdf.format(Calendar.getInstance().getTime()) + "." + String.format("%3d", millis) +
                            " [" + className + "] " + method + " > " + msg);
    }

    /**
     * instantiates a FloatDataPoint with it's associated FloatRegister, with an initial value
     * @param label the DataPoint's label
     * @param input the DataPoint's input value, true if input, false if output
     * @param value the DataPoint's initial value
     * @param address the FloatRegister address attribute
     * @param range the FloatRegister range attribute
     * @param offset the FloatRegister offset attribute
     */
    public static void createFDPAndRegister(String label, boolean input, float value, int address, int range, int offset){
        new FloatRegister(new FloatDataPoint(label, value, input), address, range, offset);
        ((FloatDataPoint)(DataPoint.getDataPointFromLabel(label))).setValue(value);
    }

    /**
     * instantiates a FloatDataPoint with it's associated FloatRegister, without an initial value
     * @param label the DataPoint's label
     * @param input the DataPoint's input value, true if input, false if output
     * @param address the FloatRegister address attribute
     * @param range the FloatRegister range attribute
     * @param offset the FloatRegister offset attribute
     */
    public static void createFDPAndRegister(String label, boolean input, int address, int range, int offset){
        new FloatRegister(new FloatDataPoint(label, input), address, range, offset);
    }

    /**
     * instantiates a BoolDataPoint with it's associated BooleanRegister, with an initial value
     * @param label the DataPoint's label
     * @param input the DataPoint's input value, true if input, false if output
     * @param value the DataPoint's initial value
     * @param address the BooleanRegister address attribute
     */
    public static void createBDPAndRegister(String label, boolean input, boolean value, int address){
        new BooleanRegister(new BoolDataPoint(label, value, input), address);
        ((BoolDataPoint)(DataPoint.getDataPointFromLabel(label))).setValue(value);
    }

    /**
     * instantiates a BoolDataPoint with it's associated BooleanRegister, without an initial value
     * @param label the DataPoint's label
     * @param input the DataPoint's input value, true if input, false if output
     * @param address the BooleanRegister address attribute
     */
    public static void createBDPAndRegister(String label, boolean input, int address){
        new BooleanRegister(new BoolDataPoint(label, input), address);
    }

    /**
     * initiate all the DataPoints with their register using the correct server address and parameters
     */
    public static void initDataPointsAndRegisters(){
        createFDPAndRegister("GRID_U_FLOAT", true, 89, 1000, 0 );
        createFDPAndRegister("BATT_P_FLOAT", true, 57, 6000, -3000);
        createFDPAndRegister("BATT_CHRG_FLOAT", true, 49, 1, 0);
        createFDPAndRegister("SOLAR_P_FLOAT", true, 61, 1500, 0);
        createFDPAndRegister("WIND_P_FLOAT", true, 53, 1000, 0);
        createFDPAndRegister("COAL_P_FLOAT", true, 81, 600, 0);
        createFDPAndRegister("COAL_AMOUNT", true, 65, 1, 0);
        createFDPAndRegister("HOME_P_FLOAT", true, 101, 1000, 0);
        createFDPAndRegister("PUBLIC_P_FLOAT", true, 97, 500,0 );
        createFDPAndRegister("FACTORY_P_FLOAT", true, 105, 2000, 0);
        createFDPAndRegister("BUNKER_P_FLOAT", true, 93, 500,0);
        createFDPAndRegister("WIND_FLOAT", true, 301, 1, 0);
        createFDPAndRegister("WEATHER_FLOAT", true, 305, 1, 0);
        createFDPAndRegister("WEATHER_FORECAST_FLOAT", true, 309, 1, 0);
        createFDPAndRegister("WEATHER_COUNTDOWN_FLOAT", true, 313, 600, 0);
        createFDPAndRegister("CLOCK_FLOAT", true, 317, 1,0);
        createFDPAndRegister("REMOTE_COAL_SP", false, 0,  209,1,0);
        createFDPAndRegister("REMOTE_FACTORY_SP", false, 0,205,1,0);
        createBDPAndRegister("REMOTE_SOLAR_SW", false, 401);
        createBDPAndRegister("REMOTE_WIND_SW", false, 405);
        createFDPAndRegister("FACTORY_ENERGY", true, 341, 3600000,0);
        createFDPAndRegister("SCORE", true, 345, 3600000,0);
        createFDPAndRegister("COAL_SP", true, 601, 1, 0);
        createFDPAndRegister("FACTORY_SP", true, 605, 1,0);
        createBDPAndRegister("SOLAR_CONNECT_SW", true, 609);
        createBDPAndRegister("WIND_CONNECT_SW", true, 613);
    }

    /**
     * Creates a JSON object that represents a BoolDataPoint
     * @param label the DataPoint's label
     * @param input the DataPoint's input value, true if input, false if output
     * @param address the BooleanRegister address attribute
     * @return temp
     */
    public static JSONObject createObj(String label, boolean input, int address){
        //BDP
        JSONObject temp = new JSONObject();
        temp.put("type","BDP");
        temp.put("label",label);
        temp.put("input",input);
        temp.put("address",address);
        return temp;
    }


    /**
     * Creates a JSON object that represents a FloatDataPoint, without an inital value
     * @param label the DataPoint's label
     * @param input the DataPoint's input value, true if input, false if output
     * @param address the FloatRegister address attribute
     * @param range the FloatRegister range attribute
     * @param offset the FloatRegister offset attribute
     * @return temp
     */
    public static JSONObject createObj(String label, boolean input, int address, int range, int offset){
        //FDP
        JSONObject temp = new JSONObject();
        temp.put("type","FDP");
        temp.put("label",label);
        temp.put("input",input);
        temp.put("address",address);
        temp.put("range",range);
        temp.put("offset",offset);
        return temp;
    }

    /**
     * Creates a JSON object that represents a FloatDataPoint, with an initial value
     *@param label the DataPoint's label
     *@param input the DataPoint's input value, true if input, false if output
     *@param value the DataPoint's initial value
     *@param address the FloatRegister address attribute
     *@param range the FloatRegister range attribute
     *@param offset the FloatRegister offset attribute
     * @return temp
     */
    public static JSONObject createObj(String label, boolean input,int value , int address, int range, int offset){
        //FDP
        JSONObject temp = new JSONObject();
        temp.put("type","FDP");
        temp.put("label",label);
        temp.put("input",input);
        temp.put("address",address);
        temp.put("range",range);
        temp.put("offset",offset);
        return temp;
    }

    /**
     * This method was called once to create the .json file that contains all the parameters
     */
    public static void createJson(){
        Utility.initDataPointsAndRegisters();

        JSONObject parser = new JSONObject();

        JSONObject modbus_param = new JSONObject();
        modbus_param.put("host","localhost");
        modbus_param.put("server_port",1502);
        parser.put("modbus",modbus_param);

        JSONObject database_param = new JSONObject();
        database_param.put("url","https://influx.sdi.hevs.ch/write?db=SIn09");
        database_param.put("userpass","SIn09:eac409529305e701e4e40bb266b949fd");
        parser.put("database",database_param);

        JSONArray registers = new JSONArray();

        registers.put(createObj("GRID_U_FLOAT", true, 89, 1000, 0 ));
        registers.put(createObj("BATT_P_FLOAT", true, 57, 6000, -3000));
        registers.put(createObj("BATT_CHRG_FLOAT", true, 49, 1, 0));
        registers.put(createObj("SOLAR_P_FLOAT", true, 61, 1500, 0));
        registers.put(createObj("WIND_P_FLOAT", true, 53, 1000, 0));
        registers.put(createObj("COAL_P_FLOAT", true, 81, 600, 0));
        registers.put(createObj("COAL_AMOUNT", true, 65, 1, 0));
        registers.put(createObj("HOME_P_FLOAT", true, 101, 1000, 0));
        registers.put(createObj("PUBLIC_P_FLOAT", true, 97, 500,0 ));
        registers.put(createObj("FACTORY_P_FLOAT", true, 105, 2000, 0));
        registers.put(createObj("BUNKER_P_FLOAT", true, 93, 500,0));
        registers.put(createObj("WIND_FLOAT", true, 301, 1, 0));
        registers.put(createObj("WEATHER_FORECAST_FLOAT", true, 309, 1, 0));
        registers.put(createObj("WEATHER_COUNTDOWN_FLOAT", true, 313, 600, 0));
        registers.put(createObj("CLOCK_FLOAT", true, 317, 1,0));
        registers.put(createObj("REMOTE_COAL_SP", false, 0, 209,1,0));
        registers.put(createObj("REMOTE_FACTORY_SP", false, 0,205,1,0));
        registers.put(createObj("REMOTE_SOLAR_SW", false, 401));
        registers.put(createObj("REMOTE_WIND_SW", false, 405));
        registers.put(createObj("FACTORY_ENERGY", true, 341, 3600000,0));
        registers.put(createObj("SCORE", true, 345, 3600000,0));
        registers.put(createObj("COAL_SP", true, 601, 1, 0));
        registers.put(createObj("SOLAR_CONNECT_SW", true, 609));
        registers.put(createObj("WIND_CONNECT_SW", true, 613));

        parser.put("registers",registers);

        StringWriter out = new StringWriter();
        parser.write(out);
        String jsonText = out.toString();
        //System.out.println(jsonText);

        FileWriter myWriter = null;
        try {
            myWriter = new FileWriter("settings.json");
            myWriter.write(jsonText);
            myWriter.close();
        } catch (IOException e) {
            System.err.println("[Utility][createJson]: error on creating json");
        }

    }

    /**
     * once the .json file is created, this method is called in the MainClass main method to set all the parameters
     * @param filename the path of the .json file
     */
    public static void readJson(String filename){

        JSONObject parser = null;

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            System.err.println("[Utility][readJson]: error on reading json file");
        }
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        String ls = System.getProperty("line.separator");
        while (true) {
            try {
                if (!((line = reader.readLine()) != null)) break;
            } catch (IOException e) {
                System.err.println("[Utility][readJson]: error on reading lines of json file");
            }
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        // delete the last new line separator
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        try {
            reader.close();
        } catch (IOException e) {
            System.err.println("[Utility][readJson]: error on closing json file");
        }

        String content = stringBuilder.toString();


        parser = new JSONObject(content);

        JSONObject modbus_param = parser.getJSONObject("modbus");
        try {
            ModbusAccessor.getInstance().connectToServer(modbus_param.getString("host"), modbus_param.getInt("server_port"));
        } catch (IOException e) {
            System.err.println("[Utility][readJson]: error on connect to modbus server : " + e.getMessage());
        }

        JSONObject database_param = parser.getJSONObject("database");
        DataBaseConnector.getInstance().changeServer(database_param.getString("url"),database_param.getString("userpass"));

        JSONArray registers = parser.getJSONArray("registers");

        for (int i = 0;i< registers.length();i++){
            JSONObject single_register = registers.getJSONObject(i);

            if(single_register.getString("type").equals("BDP")){
                createBDPAndRegister(single_register.getString("label"), single_register.getBoolean("input"), single_register.getInt("address"));
            }
            else{
                createFDPAndRegister(single_register.getString("label"), single_register.getBoolean("input"), single_register.getInt("address"), single_register.getInt("range"),single_register.getInt("offset"));
            }
        }
    }


}
