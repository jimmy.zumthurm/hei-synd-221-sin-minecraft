package ch.hevs.isi.tests;

import ch.hevs.isi.core.BoolDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

/**
 * This is test class that contains useful methods to test components separately
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class TestClass {

    public static void inputUpdate(){
        // Field updates an input

        // create 2 inputs
        new BoolDataPoint("factoryState", true, true);
        new FloatDataPoint("voltageValue", 230, true);


        // must push to DB and to WebPages
    }

    public static void outputUpdate(){
        // Web / Smart control updates an output

        // create 2 outputs
        new BoolDataPoint("factoryState2", true, false);
        new FloatDataPoint("voltageValue2", 230, false);

        // must push to DB ,to WebPages and to field
    }

    public static void smartControl(){        // Smart control control loop
    }

    public static void registersTest(){
        Utility.initDataPointsAndRegisters();
    }

    public static void main(String [ ] args) {

    }
}
