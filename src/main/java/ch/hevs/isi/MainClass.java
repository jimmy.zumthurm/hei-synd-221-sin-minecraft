package ch.hevs.isi;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.ModbusAccessor;
import ch.hevs.isi.utils.Utility;
/**
 * This class is the main class of our program, contains the main method that will be called to launch it
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class MainClass {

    public MainClass(String jsonPath){
        DataPoint.createHashMap();
        Utility.readJson(jsonPath);
        FieldConnector.getInstance();
    }

    public static void main(String [ ] args){
        String path;
        if(args.length != 1){
            path = "settings.json";     //default name
        }
        else{
            path = args[0];
        }
        MainClass run = new MainClass(path);
    }
}

