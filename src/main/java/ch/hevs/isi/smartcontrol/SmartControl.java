package ch.hevs.isi.smartcontrol;
import ch.hevs.isi.core.BoolDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

/**
 * This class describes the SmartControl component
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class SmartControl {

    private static final int FACTORY_PEAK_POWER = 1000;

    private static final int COAL_FACTORY_PEAK_POWER = 500;
    private static final int OPTIMISATION_TIME_SECONDS= 1500;
    private static final int TIME_CHANGE_BATTERY_MINMAX_SECONDS= 1200;
    private static final float FACTORY_MAX_SETPOINT = 1f;
    private static final float COAL_MIN_SETPOINT_NIGHT = 0.8f;

    private static float battery_usage_upper_limit;
    private static float battery_usage_lower_limit;

    //Control outputs
    FloatDataPoint factorySetPoint;
    FloatDataPoint coalSetPoint;
    BoolDataPoint remoteSolarSwitch;
    BoolDataPoint remoteWindSwitch;

    //Control inputs
    FloatDataPoint gridVoltage;
    //producers
    FloatDataPoint coalPower;
    FloatDataPoint solarPower;
    FloatDataPoint windPower;
    //consumers
    FloatDataPoint homePower;
    FloatDataPoint publicPower;
    FloatDataPoint factoryPower;
    FloatDataPoint bunkerPower;
    //other params
    FloatDataPoint batteryCharge;
    FloatDataPoint coalAmount;
    FloatDataPoint clock;

    private static SmartControl sc = new SmartControl();

    /**
     * returns the singleton of SmartControl class
     * @return SmartControl singleton object
     */
    public static SmartControl getInstance(){
        return sc;
    }

    /**
     * SmartControl constructor, instantiate all the inputs/outputs we use in variable for easier usage
     *
     */
    private SmartControl(){
        //instantiate all control outputs
        factorySetPoint = (FloatDataPoint)(DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"));
        remoteSolarSwitch = (BoolDataPoint)(DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW"));
        remoteWindSwitch = (BoolDataPoint)(DataPoint.getDataPointFromLabel("REMOTE_WIND_SW"));
        coalSetPoint = (FloatDataPoint)(DataPoint.getDataPointFromLabel("REMOTE_COAL_SP"));
        //instantiate all interesting inputs
        gridVoltage = (FloatDataPoint) (DataPoint.getDataPointFromLabel("GRID_U_FLOAT"));
        coalAmount = (FloatDataPoint) (DataPoint.getDataPointFromLabel("COAL_AMOUNT"));
        solarPower = (FloatDataPoint) (DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT"));
        windPower = (FloatDataPoint) (DataPoint.getDataPointFromLabel("WIND_P_FLOAT"));
        coalPower = (FloatDataPoint) (DataPoint.getDataPointFromLabel("COAL_P_FLOAT"));
        batteryCharge = (FloatDataPoint) (DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT"));
        homePower= (FloatDataPoint) (DataPoint.getDataPointFromLabel("HOME_P_FLOAT"));
        publicPower= (FloatDataPoint) (DataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT"));
        factoryPower= (FloatDataPoint) (DataPoint.getDataPointFromLabel("FACTORY_P_FLOAT"));
        bunkerPower= (FloatDataPoint) (DataPoint.getDataPointFromLabel("BUNKER_P_FLOAT"));
        clock = (FloatDataPoint)(DataPoint.getDataPointFromLabel("CLOCK_FLOAT"));
    }

    static int i=0;

    /**
     * changes the range of values considered to be acceptable for battery regulation
     */
    public void batteryMinMaxUpdater(){
        i++;
        if(i>=1800){
         //after 30 minutes, we need to reset
            i=0;
        }

        if(i>TIME_CHANGE_BATTERY_MINMAX_SECONDS&&i<OPTIMISATION_TIME_SECONDS){
            battery_usage_lower_limit=0.51f;
            battery_usage_upper_limit=0.54f;
        }
        else{
            battery_usage_lower_limit=0.3f;
            battery_usage_upper_limit=0.8f;
        }
    }

    /**
     * does the regulation of the electrical age world, reads some inputs and updates the outputs accordingly
     */
    public void controlLoop(){

        batteryMinMaxUpdater();

        float total_produced = solarPower.getValue() + coalPower.getValue() + windPower.getValue();
        float total_consumed = bunkerPower.getValue() + homePower.getValue() + publicPower.getValue()+ factoryPower.getValue();
        float diff = total_produced-total_consumed;

        float factorySetPointCommandValue=factorySetPoint.getValue();
        float coalSetPointCommandValue=coalSetPoint.getValue();
        boolean solarSwitchCommand=remoteSolarSwitch.getValue();
        boolean windSwitchCommand=remoteWindSwitch.getValue();



        if((clock.getValue()>=0&&clock.getValue()<=0.25)||(clock.getValue()>=0.75&&clock.getValue()<=1)){
            //at night : we turn off factory
            factorySetPointCommandValue = 0f;

            if(diff<0 && batteryCharge.getValue()<battery_usage_lower_limit){
                //battery is getting low and consumption is too high

                //turn on  wind, even if already on, don't need to check for solar we don't have any sun
                windSwitchCommand=true;

                if(remoteWindSwitch.getValue()==true){
                    //if wind is off, we turn it on and do nothing, if already on, we need to increase the coal factory production
                    if(coalPower.getValue()-diff < COAL_FACTORY_PEAK_POWER) {

                        coalSetPointCommandValue = (coalSetPoint.getValue()*COAL_FACTORY_PEAK_POWER - diff) / COAL_FACTORY_PEAK_POWER;

                        if(coalSetPointCommandValue>=1f){
                            coalSetPointCommandValue=1f;
                        }
                    }
                    else{
                        coalSetPointCommandValue=1f;
                    }
                }
            }

            else if(diff>0 && batteryCharge.getValue()>battery_usage_upper_limit){
                //battery is getting too high, need to lower coal, then turn off wind if necessary

                if(factoryPower.getValue()+diff < FACTORY_PEAK_POWER){
                    factorySetPointCommandValue = (factorySetPoint.getValue()*FACTORY_PEAK_POWER+diff)/FACTORY_PEAK_POWER;
                    if(factorySetPointCommandValue>1){
                        factorySetPointCommandValue=1;
                    }
                }
                else{
                    factorySetPointCommandValue=1f;
                    if(coalPower.getValue()>diff){
                        coalSetPointCommandValue = (coalSetPoint.getValue()*COAL_FACTORY_PEAK_POWER - diff) / COAL_FACTORY_PEAK_POWER;
                        if(coalSetPointCommandValue<=0){
                            coalSetPointCommandValue=0;
                        }
                    }
                    else{
                        coalSetPointCommandValue=0f;
                        //coal is off, lets turn off wind :D
                        windSwitchCommand=false;
                    }
                }
            }
            else if(total_consumed<total_produced && batteryCharge.getValue()<battery_usage_lower_limit){
                //do nothing, fine as it is
            }
            else if(total_produced<total_consumed && batteryCharge.getValue()>battery_usage_upper_limit){
                //do nothing, fine as it is
            }
            else{
                //in this case, we are in between the battery lower and upper limit

                windSwitchCommand=true;

                //charging battery
                if(diff>0){

                    coalSetPointCommandValue=coalSetPoint.getValue()-0.05f;

                    if(coalSetPointCommandValue<=COAL_MIN_SETPOINT_NIGHT){
                        coalSetPointCommandValue=COAL_MIN_SETPOINT_NIGHT;
                    }
                }
                //emptying battery
                else{

                    coalSetPointCommandValue=coalSetPoint.getValue()+0.1f;

                    if(coalSetPointCommandValue>1f){
                        coalSetPointCommandValue=1f;
                    }
                }
            }
        }

        else{
            //it's daylight, we want to save coal for the night
            coalSetPointCommandValue = 0f;

            if(diff<0 && batteryCharge.getValue()<battery_usage_lower_limit){
                //battery is getting low and consumption is too high

                //turn on solar and wind, even if already on
                solarSwitchCommand=true;
                windSwitchCommand=true;

                if(remoteSolarSwitch.getValue()==true && remoteWindSwitch.getValue()==true){
                    //we only make changes to factory set points if solar and wind is already on, if not, we just turn them on and do nothing else

                    if(factoryPower.getValue()+diff > 0){
                        //we can reduce the factory consumption by the difference between consumed and produced and get back to stability
                        factorySetPointCommandValue = (factorySetPoint.getValue()*FACTORY_PEAK_POWER+diff)/FACTORY_PEAK_POWER;

                        if(factorySetPointCommandValue>FACTORY_MAX_SETPOINT){
                            factorySetPointCommandValue=FACTORY_MAX_SETPOINT;
                        }
                        if(factorySetPointCommandValue<0){
                            factorySetPointCommandValue=0;
                        }
                    }

                    else
                    //no other choice than turning it completely off
                    {
                        factorySetPointCommandValue=0f;
                    }
                }
            }

            else if(diff>0 && batteryCharge.getValue()>battery_usage_upper_limit){
                //battery is getting too high

                if(factoryPower.getValue()+diff < FACTORY_PEAK_POWER){
                    factorySetPointCommandValue = (factorySetPoint.getValue()*FACTORY_PEAK_POWER+diff)/FACTORY_PEAK_POWER;
                    if(factorySetPointCommandValue>FACTORY_MAX_SETPOINT){
                        factorySetPointCommandValue=FACTORY_MAX_SETPOINT;
                    }
                }
                else{
                    factorySetPointCommandValue=FACTORY_MAX_SETPOINT;
                    windSwitchCommand = false;
                }
            }
            else if(total_consumed<total_produced && batteryCharge.getValue()<battery_usage_lower_limit){
                //do nothing, fine as it is
            }
            else if(total_produced<total_consumed && batteryCharge.getValue()>battery_usage_upper_limit){
                //do nothing, fine as it is
            }
            else{
                //in this case, we are in between the battery lower and upper limit

                windSwitchCommand=true;

                //charging battery
                if(diff>0){

                    factorySetPointCommandValue=factorySetPoint.getValue()+0.05f;

                    if(factorySetPointCommandValue>FACTORY_MAX_SETPOINT){
                        factorySetPointCommandValue=FACTORY_MAX_SETPOINT;
                    }
                }
                //emptying battery
                else{
                    factorySetPointCommandValue=factorySetPoint.getValue()-0.05f;

                    if(factorySetPointCommandValue<0){
                        factorySetPointCommandValue=0;
                    }
                }
            }
        }

        //System.out.println("-------------------------------------------------------------------------------");
        //System.out.println("Coal setpoint : "+coalSetPointCommandValue);
        //System.out.println("Factory setpoint : "+factorySetPointCommandValue);
        //System.out.println("Solar switch : "+solarSwitchCommand);
        //System.out.println("Wind switch : "+windSwitchCommand);
        //System.out.println("-------------------------------------------------------------------------------");


        coalSetPoint.setValue(coalSetPointCommandValue);
        factorySetPoint.setValue(factorySetPointCommandValue);
        remoteSolarSwitch.setValue(solarSwitchCommand);
        remoteWindSwitch.setValue(windSwitchCommand);
    }
}
