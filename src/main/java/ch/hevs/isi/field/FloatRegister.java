package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Vector;


/**
 * This class describes the FloatRegister component
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class FloatRegister {
    private DataPoint floatDataPoint;
    private int address;
    private float range;
    private float offset;
    private static Vector<FloatRegister> allFloatRegister = new Vector<>();

    /**
     * FloatRegister constructor
     * @param floatDataPoint the BooleanDataPoint object to be linked with the register
     * @param address the Modbus address related to the data represented by the DataPoint
     * @param range the number that multiplies the float data that is between 0 and 1 to have it's real value
     * @param offset the offset that needs to be applied to the float data
     */
    public FloatRegister(DataPoint floatDataPoint,int address,float range,float offset){
        this.floatDataPoint = floatDataPoint;
        this.address = address;
        this.range = range;
        this.offset = offset;

        //add to vector
        allFloatRegister.add(this);
    }

    /**
     *  Reads the value in the server's register at the right address  through Modbus and sets it in the FloatDataPoint
     */
    public void read() {
        float value;

        try {
            value = ModbusAccessor.getInstance().readFloat(address);

            // value modifiers
            value = value*range + offset;
            ((FloatDataPoint)floatDataPoint).setValue(value);
        } catch (Exception e) {
            System.err.println("[FloatRegister][read]: "+e.getMessage());
        }
    }

    /**
     * Writes the value of the FloatDataPoint in the server's register through Modbus
     * @throws Exception
     */
    public void write() throws Exception {
        float value;
        value = ((FloatDataPoint)floatDataPoint).getValue();

        // value modifiers
        value = (value-offset)/range;

        if(floatDataPoint.getInput()){
            throw new Exception("Cannot write a DataPoint(label: "+floatDataPoint.getLabel()+") set as input");
        }

        ModbusAccessor.getInstance().writeFloat(value,address);
    }

    /**
     * Static method that returns the FloatRegister that contains the FloatDataPoint fdp param, or null if not found
     * @param fdp a FloatDataPoint object
     * @return  the FloatRegister that contains the FloatDataPoint fdp param, or null if not found
     */
    public static FloatRegister getRegisterFromDataPoint(FloatDataPoint fdp){
        for(FloatRegister f : allFloatRegister){
            if(f.floatDataPoint.equals(fdp)){
                return f;
            }
        }
        return null;
    }

    /**
     * Static method that calls the read method on all the FloatRegisters     *
     */
    public static void poll(){
        for(FloatRegister f : allFloatRegister){
            f.read();
        }
    }

    /**
     * returns the static vector that contains all the FloatRegister objects
     * @return allFloatRegister
     */
    public static Vector<FloatRegister> getAllFloatRegister() {
        return allFloatRegister;
    }

    /**
     * returns the DataPoint attribute of the class
     * @return floatDataPoint
     */
    public DataPoint getFloatDataPoint() {
        return floatDataPoint;
    }
}
