package ch.hevs.isi.field;

import ch.hevs.isi.core.BoolDataPoint;
import ch.hevs.isi.core.DataPoint;

import java.util.Vector;

/**
 * This class describes the BooleanRegister component
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class BooleanRegister {
    private DataPoint booleanDataPoint;
    private int address;
    private static Vector<BooleanRegister> allBooleanRegister = new Vector<>();

    /**
     * BooleanRegister constructor
     * @param booleanDataPoint the BooleanDataPoint object to be linked with the register
     * @param address the Modbus address related to the data represented by the DataPoint
     */
    public BooleanRegister(DataPoint booleanDataPoint,int address){
        this.booleanDataPoint = booleanDataPoint;
        this.address = address;

        //add to vector
        allBooleanRegister.add(this);
    }

    /**
     * Reads the value in the server's register at the right address through Modbus and sets it in the BoolDataPoint
     */
    public void read() {
        boolean value;
        try {
            value = ModbusAccessor.getInstance().readBoolean(address);
            ((BoolDataPoint)booleanDataPoint).setValue(value);
        } catch (Exception e) {
            System.err.println("[BooleanRegister][read]: "+e.getMessage());
        }
    }

    /**
     * Write the value of the BoolDataPoint in the server's register through Modbus
     * @throws Exception
     */
    public void write() throws Exception {
        boolean value;
        value = ((BoolDataPoint)booleanDataPoint).getValue();

        if(booleanDataPoint.getInput()){
            throw new Exception("Cannot write a DataPoint(label: "+booleanDataPoint.getLabel()+") set as input");
        }

        ModbusAccessor.getInstance().writeBoolean(value,address);
    }

    /**
     * Static method that returns the BooleanRegister that contains the bool param, or null if not found
     * @param bool a BoolDataPoint object
     * @return the BooleanRegister that contains the bool param, or null if not found
     */
    public static BooleanRegister getRegisterFromDataPoint(BoolDataPoint bool){
        for(BooleanRegister b : allBooleanRegister){
            if(b.booleanDataPoint.equals(bool)){
                return b;
            }
        }
        return null;
    }

    /**
     * Static method that calls the read method on all the BooleanRegisters
     */
    public static void poll(){
        for(BooleanRegister b : allBooleanRegister){
            b.read();
        }
    }

    /**
     * returns the static vector that contains all the BooleanRegister objects
     * @return allBooleanRegister
     */
    public static Vector<BooleanRegister> getAllBooleanRegister() {
        return allBooleanRegister;
    }

    /**
     * returns the DataPoint attribute of the class
     * @return booleanDataPoint
     */
    public DataPoint getBooleanDataPoint() {
        return booleanDataPoint;
    }
}
