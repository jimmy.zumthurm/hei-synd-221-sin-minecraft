package ch.hevs.isi.field;
import ch.hevs.isi.utils.Utility;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ModbusAccessor extends Socket {

    //constant values
    public static final int TRANSACTION_IDENTIFIER = 1000;
    public static final int MBAP_LENGTH = 7;
    public static final int WRITE_MULTIPLE_REGISTERS_RESPONSE_PDU_LENGTH = 5;
    public static final int WRITE_MULTIPLE_REGISTERS_REQUEST_PDU_LENGTH = 10;
    public static final int READ_MULTIPLE_REGISTERS_REQUEST_PDU_LENGTH = 5;
    public static final int READ_MULTIPLE_REGISTERS_RESPONSE_PDU_LENGTH = 6;

    //class attributes
    private InputStream in;
    private OutputStream out;
    private boolean connectionEstablished;
    private static ModbusAccessor ma = new ModbusAccessor();


    /**
     * returns singleton from the ModbusAccessor class
     * @return singleton from the ModbusAccessor class
     */
    public static ModbusAccessor getInstance(){
        return ma;
    }

    private ModbusAccessor() {
        super();
        connectionEstablished = false;
    }

    /**
     *
     * @param host the name of the host as String
     * @param server_port the number of the server port to connect to
     * @throws IOException
     */
    public void connectToServer(String host, int server_port) throws IOException {
        connect(new InetSocketAddress(host, server_port));
        out = this.getOutputStream();
        in = this.getInputStream();
        connectionEstablished = true;
    }
    /**
     *
     * @param value the float value to write in the registers
     * @param address address of the modbus registers
     * @throws Exception
     */
    public void writeFloat(float value, int address) throws Exception {

        //throws error if connection is not initialized when the method is called
        if(connectionEstablished==false){
            throw new Exception("Connection with the server is not initialized");
        }

        //constant values
        final int length = WRITE_MULTIPLE_REGISTERS_REQUEST_PDU_LENGTH+1;
        final int total_request_length= WRITE_MULTIPLE_REGISTERS_REQUEST_PDU_LENGTH + MBAP_LENGTH;
        final int total_response_length  = WRITE_MULTIPLE_REGISTERS_RESPONSE_PDU_LENGTH+MBAP_LENGTH;

        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(total_request_length);
        buffer.order(ByteOrder.BIG_ENDIAN);

        //adds the MBAP to the ByteBuffer
        mbap(length,buffer );

        //adds the PDU to the ByteBuffer
        buffer.put((byte)0x10); //function code
        buffer.putShort((short)address);
        buffer.putShort((short)2);
        buffer.put((byte)4);
        buffer.putFloat(value);

        //write on OutPutStream and flush
        out.write(buffer.array());
        out.flush();

        //tmp array of bytes that comes from the inputstream
        byte[] tmp = new byte[total_response_length];
        in.read(tmp, 0, total_response_length);

        //converts the array to a ByteBuffer
        ByteBuffer temp = ByteBuffer.wrap(tmp,0,total_response_length);

        //checks if the transaction identifier of the sent packed and the received one is the same
        if(buffer.get(0)!=temp.get(0)){
            throw new Exception("Transaction identifier doesn't match");
        }

        //clear the 2 ByteBuffers
        buffer.clear();
        temp.clear();

    }

    /**
     *
     * @param value boolean value to write in the registers
     * @param address address of the modbus registers
     * @throws Exception
     */
    public void writeBoolean(boolean value, int address) throws Exception{
        if(value)
            writeFloat(1f, address );
        else
            writeFloat(0f, address );
    }

    /**
     * @param address address of the modbus registers to read
     * @return float value at the desired address
     * @throws Exception
     */
    public float readFloat(int address) throws Exception {

        //throws error if connection is not initialized when the method is called
        if(connectionEstablished==false){
            throw new Exception("Connection with the modbus server is not initialized");
        }

        //return valuehahah
        float retval;

        //constants declaration
        final int length = READ_MULTIPLE_REGISTERS_REQUEST_PDU_LENGTH+1;
        final int total_request_length= READ_MULTIPLE_REGISTERS_REQUEST_PDU_LENGTH + MBAP_LENGTH;
        final int total_response_length  = READ_MULTIPLE_REGISTERS_RESPONSE_PDU_LENGTH+MBAP_LENGTH;

        ByteBuffer buffer;
        buffer = ByteBuffer.allocate(total_request_length);
        buffer.order(ByteOrder.BIG_ENDIAN);

        //adds the MBAP to the ByteBuffer
        mbap(length, buffer);

        //adds the PDU to the ByteBuffer
        buffer.put((byte)0x03); //function code
        buffer.putShort((short)address); //address of the register
        buffer.putShort((short)2); //number of registers to read

        //converts buffer to an array of bytes and writes it in the outputstream
        out.write(buffer.array());
        out.flush();

        //tmp array of bytes that comes from the inputstream
        byte[] tmp = new byte[total_response_length];
        in.read(tmp, 0, total_response_length);

        //converts the array to a ByteBuffer
        ByteBuffer temp = ByteBuffer.wrap(tmp,0,total_response_length);

        //checks if the transaction identifier of the sent packed and the received one is the same
        if(buffer.getShort(0)!=temp.getShort(0)){
            throw new Exception("Transaction identifier doesn't match");
        }

        //read float value at the correct position
        retval = temp.getFloat(MBAP_LENGTH+2);

        //clear the 2 ByteBuffers
        buffer.clear();
        temp.clear();

        return retval;
    }

    /**
     *
     * @param address address of the modbus registers to read
     * @return boolean value at the desired address
     * @throws Exception
     */
    public boolean readBoolean(int address) throws Exception {
        if(readFloat(address)==0f)
            return false;
        else
            return true;
    }

    private void mbap(int length, ByteBuffer buffer){

        buffer.putShort((short)(TRANSACTION_IDENTIFIER));
        buffer.putShort((short) 0);
        buffer.putShort((short)length);
        buffer.put((byte)1);
    }

    public static void main(String [ ] args) throws Exception {

        ma.connectToServer("localhost", 1502);

        while(true) {
            Utility.waitSomeTime(1000);
            ma.writeBoolean(true, 401);
            //System.out.println(ma.readBoolean(401));
        }
    }
}
