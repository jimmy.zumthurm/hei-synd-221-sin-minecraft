package ch.hevs.isi.field;

import ch.hevs.isi.Connector;
import ch.hevs.isi.core.BoolDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Timer;

/**
 * This class describes the FieldConnector component
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class FieldConnector implements Connector {
    private static final int POLLTIMER_PERIOD = 1000;
    private static final int POLLTIMER_DELAY = 1000;

    private static FieldConnector fc = new FieldConnector();
    private Timer pollTimer;

    /**
     * FieldConnector constructor
     */
    private FieldConnector(){
        //INIT
        pollTimer = new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(), POLLTIMER_DELAY, POLLTIMER_PERIOD);
    }

    /**
     * returns singleton from the FieldConnector class
     * @return singleton from the FieldConnector class
     */
    public static FieldConnector getInstance(){
        return fc;
    }

    /**
     * calls the pushToField method when a DataPoint object's value gets changed
     * @param p the DataPoint that had its value changed
     */
    public void onNewValue(DataPoint p){
        String label = p.getLabel();

        // select the good type
        if(p instanceof FloatDataPoint){
            // dataPoint is a float
            float value = ((FloatDataPoint)p).getValue();
            try {
                pushToField(label,value);
            } catch (Exception e) {
                System.err.println("[FieldConnector][onNewValue]: error on pushing float with label: "+label);
            }
        }
        else{
            // dataPoint is a boolean
            boolean value = ((BoolDataPoint)p).getValue();
            try {
                pushToField(label,value);
            } catch (Exception e) {
                System.err.println("[FieldConnector][onNewValue]: error on pushing boolean with label: "+label);
            }
        }
    }
    /**
     * pushes the label and value of the FloatDataPoint object to the field
     * @param label name of the FloatDataPoint object
     * @param value value of the FloatDataPoint object
     */
    private void pushToField(String label, float value) throws Exception {
        //Test push to DB
        FloatRegister.getRegisterFromDataPoint((FloatDataPoint)(DataPoint.getDataPointFromLabel(label))).write();
        System.out.println("[FieldConnector][PUSH]: label : " +label+ " with value = " +value+ " pushed to Field");
    }
    /**
     * pushes the label and value of the BoolDataPoint object to the field
     * @param label name of the BoolDataPoint object
     * @param value value of the BoolDataPoint object
     */
    private void pushToField(String label, boolean value) throws Exception {
        //Test push to DB
        BooleanRegister.getRegisterFromDataPoint((BoolDataPoint)(DataPoint.getDataPointFromLabel(label))).write();
        System.out.println("[FieldConnector][PUSH]: label : " +label+ " with value = " +value+ " pushed to Field");
    }
}
