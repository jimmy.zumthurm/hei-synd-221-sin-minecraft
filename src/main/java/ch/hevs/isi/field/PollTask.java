package ch.hevs.isi.field;

import ch.hevs.isi.smartcontrol.SmartControl;

import java.util.TimerTask;
public class PollTask extends TimerTask {

    PollTask() {
        super();
    }

    /**
     * calls the poll static method and the SmartControl singleton's controlLoop method
     */
    public void run() {
        //System.out.println("------------------------------------------------------");

        FloatRegister.poll();
        BooleanRegister.poll();

        SmartControl.getInstance().controlLoop();
    }
}