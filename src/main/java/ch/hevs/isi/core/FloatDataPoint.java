package ch.hevs.isi.core;

import ch.hevs.isi.database.DataBaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;


/**
 * This class describes the core component FloatDataPoint, which inherits from DataPoint. Specifies DataPoints
 * that have a <code>Float</code> value
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class FloatDataPoint extends DataPoint{
    private float value;

    /**
     * FloatDataPoint constructor with 3 parameters, also calls DataPoint super constructor with label and input
     * parameters
     *
     * @param label the name of the FloatDataPoint object
     * @param value value of the FloatDataPoint object, either true(on) or false(off)
     * @param input this parameter is true of the object is used as an input, false if it's used as an output
     */
    public FloatDataPoint(String label, float value, boolean input){
        super(label, input);
        if(!input){
            this.value = value;
        }
    }

    /**
     * FloatDataPoint constructor
     * @param label the name of the DataPoint object
     * @param input this parameter is true of the object is used as an input, false if it's used as an output
     */
    public FloatDataPoint(String label, boolean input){
        super(label, input);
    }

    /**
     * sets the value of the <code>Float</code> attribute , and also calls the onNewValue methods of the singletons from
     * the WebConnector, DataBaseConnector, FieldConnector and DataBaseConnector classes, depending on whether
     * the FloatDataPoint object is an input or an output
     *
     * @param value new value of the FloatDataPoint object
     */
    public void setValue(float value){
        this.value = value;

        if(input){
            WebConnector.getInstance().onNewValue(this);
            DataBaseConnector.getInstance().onNewValue(this);
        }
        else{
            WebConnector.getInstance().onNewValue(this);
            DataBaseConnector.getInstance().onNewValue(this);
            FieldConnector.getInstance().onNewValue(this);
        }
    }

    /**
     * returns the value attribute of the FloatDataPoint object
     *
     * @return value
     */
    public float getValue(){
        return value;
    }
}
