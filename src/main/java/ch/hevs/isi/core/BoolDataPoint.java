package ch.hevs.isi.core;

import ch.hevs.isi.database.DataBaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

/**
 * This class describes the core component BoolDataPoint, which inherits from DataPoint. Specifies DataPoints
 * that can only have a boolean state as value
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class BoolDataPoint extends DataPoint{
    private boolean state;

    /**
     * BoolDataPoint constructor with 3 parameters, also calls DataPoint super constructor with label and input
     * parameters
     *
     * @param label the name of the BoolDataPoint object
     * @param state state of the BoolDataPoint object, either true(on) or false(off)
     * @param input this parameter is true of the object is used as an input, false if it's used as an output
     */
    public BoolDataPoint(String label, boolean state, boolean input){
        super(label, input);
        if(!input){
            this.state = state;
        }
    }

    /**
     * BoolDataPoint constructor
     * @param label the name of the DataPoint object
     * @param input this parameter is true of the object is used as an input, false if it's used as an output
     */
    public BoolDataPoint(String label, boolean input){
        super(label, input);
    }


    /**
     * sets the value of the boolean parameter state, and also calls the onNewValue methods of the singletons from
     * the WebConnector, DataBaseConnector, FieldConnector and DataBaseConnector classes, depending on whether
     * the BoolDataPoint object is an input or an output
     *
     * * @param state new state of the BoolDataPoint object
     * @param state new state of the BoolDataPoint object
     */
    public void setValue(boolean state){
        this.state = state;

        if(input){
            WebConnector.getInstance().onNewValue(this);
            DataBaseConnector.getInstance().onNewValue(this);
        }
        else {
            WebConnector.getInstance().onNewValue(this);
            FieldConnector.getInstance().onNewValue(this);
            DataBaseConnector.getInstance().onNewValue(this);
        }
    }

    /**
     * returns the state attribute of the BoolDataPoint object
     *
     * @return state
     */
    public boolean getValue(){
        return state;
    }
}
