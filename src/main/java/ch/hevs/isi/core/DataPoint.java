package ch.hevs.isi.core;

import java.util.HashMap;


/**
 * This class describes the core component DataPoint and how to interact with it
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public abstract class DataPoint {
    protected String label;
    protected boolean input;
    private static HashMap<String, DataPoint> map;

    /**
     * DataPoint constructor with 2 parameters
     *
     * @param label the name of the DataPoint object
     * @param input this parameter is true of the object is used as an input, false if it's used as an output
     */
    public DataPoint(String label, boolean input){
        this.input = input;
        this.label = label;
        map.put(label, this);
    }

    /**
     * Returns the object's label attribute as a String
     *
     * @return label the name of the DataPoint object
     */
    public String getLabel(){
        return label;
    }

    /**
     * Initialize the HashMap that contains all the DataPoint objects
     */
    public static void createHashMap(){
        map = new HashMap<>();
    }

    /**
     * get the DataPoint object from the HashMap by using it's key
     *
     * @param label the name of the DataPoint object
     * @return map.get(label)
     */
    public static DataPoint getDataPointFromLabel(String label){
        return map.get(label);
    }

    /**
     * sets as input or as output
     *
     * @param input this parameter is true of the object is used as an input, false if it's used as an output
     */
    public void setInput(boolean input){
        this.input = input;
    }

    /**
     * Returns the input attribute value as boolean
     * @return input
     */
    public boolean getInput(){
        return this.input;
    }


}
