package ch.hevs.isi;


import ch.hevs.isi.core.DataPoint;

public interface Connector {
    public void onNewValue(DataPoint p) throws Exception;
}
