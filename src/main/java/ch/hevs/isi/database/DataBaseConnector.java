package ch.hevs.isi.database;

import ch.hevs.isi.Connector;
import ch.hevs.isi.core.BoolDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;


/**
 * This class describes the DataBaseConnector component
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class DataBaseConnector implements Connector {

    private static DataBaseConnector dc = new DataBaseConnector();

    URL url ;

    String userpass;

    /**
     * DataBaseConnector constructor
     */
    private DataBaseConnector(){
        //INIT
        // nothing for the moment
        try {
            url = new URL("https://influx.sdi.hevs.ch/write?db=SIn09");
            userpass="SIn09:eac409529305e701e4e40bb266b949fd";
        } catch (MalformedURLException e) {
            System.err.println("[DataBaseConnector][constructor]: error on database url");
        }
    }

    /**
     * returns singleton from the DataBaseConnector class
     * @return singleton from the DataBaseConnector class
     */
    public static DataBaseConnector getInstance(){
        return dc;
    }

    /**
     * calls the pushToDB method when a DataPoint object's value gets changed
     * @param p the DataPoint that had its value changed
     */
    public void onNewValue(DataPoint p){
        String label = p.getLabel();
        // select the good type
        if(p instanceof FloatDataPoint){
            // dataPoint is a float
            float value = ((FloatDataPoint)p).getValue();
            try {
                pushToDB(label,value);
            } catch (Exception e) {
                System.err.println("[DataBaseConnector][onNewValue]: error on pushing float to DB : " + e.getMessage());
            }
        }
        else{
            // dataPoint is a boolean
            boolean value = ((BoolDataPoint)p).getValue();
            try {
                pushToDB(label,value);
            } catch (Exception e) {
                System.err.println("[DataBaseConnector][onNewValue]: error on pushing boolean to DB : " + e.getMessage());
            }
        }
    }

    /**
     * changes the server
     * @param url the server's URL
     * @param userpass the server's userpass
     */
    public void changeServer(String url, String userpass){
        try {
            this.url = new URL(url);
            this.userpass = userpass;
        } catch (MalformedURLException e) {
            System.err.println("[DataBaseConnector][changeServer]: error on database url");
        }
    }

    /**
     * prepares and send HTML stream to the DB
     * @param label the DataPoint's label
     * @param value the DataPoint's value
     * @throws Exception
     */
    private void post(String label, float value) throws Exception{

        HttpURLConnection connection= (HttpURLConnection)url.openConnection();

        String encoding= Base64.getEncoder().encodeToString(userpass.getBytes());
        connection.setRequestProperty ("Authorization", "Basic " + encoding);

        connection.setRequestProperty("Content-Type", "binary/octet-stream");
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

        writer.write(label+" value="+value);
        writer.flush();

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        //System.out.println(label+" value="+value);

        int responseCode = connection.getResponseCode();

        while ((in.readLine()) != null) {}

        if(responseCode!=HttpURLConnection.HTTP_NO_CONTENT){
            throw new Exception("Error : wrong status code");
        }

        connection.disconnect();

    }

    /**
     * pushes the label and value of the FloatDataPoint object to the database
     * @param label name of the FloatDataPoint object
     * @param value value of the FloatDataPoint object
     */
    private void pushToDB(String label, float value) throws Exception {

        //Test push to DB
        post(label,value);
        System.out.println("[DataBaseConnector][PUSH]: label : " +label+ " with value = " +value+ " pushed to DB");
    }
    /**
     * pushes the label and value of the BoolDataPoint object to the database
     * @param label name of the BoolDataPoint object
     * @param value value of the BoolDataPoint object
     */
    private void pushToDB(String label, boolean value) throws Exception{
        //Test push to DB
        if(value){
            post(label, 1f);
        }
        else{
            post(label, 0f);
        }
        System.out.println("[DataBaseConnector][PUSH]: label : " +label+ " with value = " +value+ " pushed to DB");
    }
}
