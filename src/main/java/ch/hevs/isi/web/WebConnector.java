package ch.hevs.isi.web;

import ch.hevs.isi.Connector;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.BoolDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FloatRegister;
import ch.hevs.isi.utils.Utility;
import jdk.jfr.events.ExceptionThrownEvent;
import jdk.nashorn.internal.parser.JSONParser;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.rmi.CORBA.Util;
import java.net.InetSocketAddress;

/**
 * This class describes the WebConnector component
 *
 * @author Jimmy Zumthurm
 * @author Aurélien Héritier
 */
public class WebConnector extends WebSocketServer implements Connector {

    private static WebConnector wc = new WebConnector();

    private static final String host = "localhost";
    private static final int  port = 8888;

    /**
     * WebConnector constructor
     */
    private WebConnector(){
        super(new InetSocketAddress(host, port));
        //INIT

        start();
    }

    /**
     * returns singleton from the WebConnector class
     * @return singleton from the WebConnector class
     */
    public static WebConnector getInstance(){
        return wc;
    }

    /**
     * calls the pushToWebPages method when a DataPoint object's value gets changed
     * @param p the DataPoint that had its value changed
     */
    public void onNewValue(DataPoint p){
        String label = p.getLabel();

        // select the good type
        if(p instanceof FloatDataPoint){
            // dataPoint is a float
            float value = ((FloatDataPoint)p).getValue();
            pushToWebPages(label,value);
        }
        else{
            // dataPoint is a boolean
            boolean value = ((BoolDataPoint)p).getValue();
            pushToWebPages(label,value);
        }
    }

    /**
     * pushes the label and value of the FloatDataPoint object to the web pages
     * @param label name of the FloatDataPoint object
     * @param value value of the FloatDataPoint object
     */
    private void pushToWebPages(String label, float value){
        //Test push to DB
        broadcast(label+"="+value);
        System.out.println("[WebConnector][PUSH]: label : " +label+ " with value = " +value+ " pushed to Web pages");
    }
    /**
     * pushes the label and value of the BoolDataPoint object to the web pages
     * @param label name of the BoolDataPoint object
     * @param value value of the BoolDataPoint object
     */
    private void pushToWebPages(String label, boolean value){
        //Test push to DB
        // broadcast
        broadcast(label+"="+value);
        System.out.println("[WebConnector][PUSH]: label : " +label+ " with value = " +value+ " pushed to Web pages");
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        // welcomwe message
        // send first values
        webSocket.send("Welcome to the best server eveeeeeeeeeeeer!"); //This method sends a message to the new client
        //broadcast( "new connection: " + clientHandshake.getResourceDescriptor() ); //This method sends a message to all clients connected
        System.out.println("[WebConnector][onOpen]: label : new connection to " + webSocket.getRemoteSocketAddress());

        // resend all
        for (FloatRegister fr : FloatRegister.getAllFloatRegister()){
            webSocket.send(fr.getFloatDataPoint().getLabel() +"="+((FloatDataPoint)fr.getFloatDataPoint()).getValue());
        }

        for (BooleanRegister br : BooleanRegister.getAllBooleanRegister()){
            webSocket.send(br.getBooleanDataPoint().getLabel() +"="+((BoolDataPoint)br.getBooleanDataPoint()).getValue());
        }
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        // close
        System.out.println("[WebConnector][onClose]: label : closed " + webSocket.getRemoteSocketAddress() + " with exit code " + i + " additional info: " + s);
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        // set value
        String[] parts = s.split("=");

        if(parts[1].equals("true") || parts[1].equals("false")){
            ((BoolDataPoint)DataPoint.getDataPointFromLabel(parts[0])).setValue(Boolean.parseBoolean(parts[1]));
        }
        else{
            try{
                ((FloatDataPoint)DataPoint.getDataPointFromLabel(parts[0])).setValue(Float.parseFloat(parts[1]));
            }
            catch(Exception e){
                System.err.println("[WebConnector][onMessage]: error on parsing float");
            }
        }

        //System.out.println("received message from "	+ webSocket.getRemoteSocketAddress() + ": " + s);
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        // nothing
        System.out.println("[WebConnector][onError]: label : an error occurred on connection " + webSocket.getRemoteSocketAddress()  + ":" + e);
    }

    @Override
    public void onStart() {
        System.out.println("[WebConnector][onStart]: server started successfully");
    }

    public static void main(String[] args) {
        WebConnector.getInstance();
        DataPoint.createHashMap();

        Utility.readJson("settings.json");
        //Utility.initDataPointsAndRegisters();
    }
}
