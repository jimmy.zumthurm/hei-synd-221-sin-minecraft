// Copyright (C) 2017 Hes-so Valais//Wallis, HEI, Sion. All rights reserved.
//
// Author: Patrice Rudaz (poatrice.rudaz@hevs.ch)

// *****  *************************************************************
var wsocket;

function connect() {         
	wsocket = new WebSocket("ws://localhost:8888");       
	wsocket.onmessage = onMessage;
	wsocket.onOpen    = onOpen;
}

function onMessage(evt) {             
	var str = evt.data;
	if (str.startsWith("Welcome")) {
		document.getElementById("wsMessage").innerHTML=str;
	} else {
		if (str.includes("=")) {
			var cmds     = str.split("=");
			var cmdLabel = cmds[0];
			var cmdValue = cmds[1];
			var factor   = 1;
			var offset   = 0;
			var elem     = document.getElementById(cmdLabel);
			
			if ((cmdLabel.includes("_SW") && elem != null)) {
				elem.checked = (cmdValue == "true");
				return;
			}
			
			if (elem != null) {
				elem.value=((parseFloat(cmdValue) * factor) + offset).toFixed( 2 );
			}
		}
	}
}

function onOpen() {

}

function toggleSwitch(cb) {
	if (cb != null) {
		var label = cb.id;
		var value = cb.checked;

		wsocket.send(label + "=" + value);
	}
}

function sendSP(btn) {
	if (btn != null) {
		var label = btn.id;
		var value = document.getElementById(label).value;

		wsocket.send(label + "=" + value);
	}
}

